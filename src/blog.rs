use std::error::Error;

#[path = "lib.rs"]
mod lib;

pub fn main() -> Result<(), Box<dyn Error>> {
    lib::main()?;

    println!("");
    println!("blog has been generated; you can now serve its content by running\n\
              {INDENT}python3 -m http.server --directory {ROOT}/public\n\
              or running:\n\
              {INDENT}cargo run -p serve\n\
              or you can read it directly by opening a web browser.",
             ROOT=env!("CARGO_MANIFEST_DIR"), INDENT="    ");

    Ok(())
}
