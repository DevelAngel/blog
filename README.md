# DevelAngel's Blog

This is my blog.

It's implemented as a small static site generator,
derived from the [blog.rust-lang.org](https://github.com/rust-lang/blog.rust-lang.org) project.

## Building

To build the site locally:

```console
> cargo run
```

You could do it in release mode if you'd like, but it's pretty fast in debug.

From there, the generated HTML will be in a `public` directory.
Open `public/rust/index.html` in your web browser to view the site.

```console
> firefox public/rust/index.html
```

You can also run a server, if you need to preview your changes on a different machine:

```console
> cargo run -p serve
Serving on: http://192.168.123.45:8000
```

## License

Like everything in Rust, the static site generator is dual-licensed
[MIT](http://opensource.org/licenses/MIT) /
[Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0).

The fonts are licensed under the
[SIL Open Font License (OFL)](http://scripts.sil.org/OFL).

The blog posts are licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License (CC BY-NC-SA 4.0)](http://creativecommons.org/licenses/by-nc-sa/4.0/).

Third-party logos may be subject to third-party copyrights and trademarks,
and are not available under the same license as the rest of the code.
